[33mcommit af1da142d30f5658d9ea6664b32c5c13f2d107ca[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: rishi.vanarse <rishi.vanarse@mosambee.com>
Date:   Fri May 14 11:31:31 2021 +0530

    third

[1mdiff --git a/Commands_output.txt b/Commands_output.txt[m
[1mnew file mode 100644[m
[1mindex 0000000..7f5d7f6[m
[1m--- /dev/null[m
[1m+++ b/Commands_output.txt[m
[36m@@ -0,0 +1,60 @@[m
[32m+[m[32m[rishi@master1 ~]$ NAME                  CIDR            NAT    IPIPMODE   VXLANMODE   DISABLED   SELECTOR^C[m
[32m+[m[32m[rishi@master1 ~]$ clear[m
[32m+[m[32m[rishi@master1 ~]$ sudo calicoctl get ippool -o wide[m
[32m+[m[32mNAME                  CIDR            NAT    IPIPMODE   VXLANMODE   DISABLED   SELECTOR[m
[32m+[m[32mdefault-ipv4-ippool   172.16.0.0/16   true   Always     Never       false      all()[m
[32m+[m
[32m+[m[32m[rishi@master1 ~]$ kubeadm config view | grep -i subnet[m
[32m+[m[32mCommand "view" is deprecated, This command is deprecated and will be removed in a future release, please use 'kubectl get cm -o yaml -n kube-system kubeadm-config' to get the kubeadm config directly.[m
[32m+[m[32mfailed to load admin kubeconfig: open /etc/kubernetes/admin.conf: permission denied[m
[32m+[m[32mTo see the stack trace of this error execute with --v=5 or higher[m
[32m+[m[32m[rishi@master1 ~]$ sudo kubeadm config view | grep -i subnet[m
[32m+[m[32mCommand "view" is deprecated, This command is deprecated and will be removed in a future release, please use 'kubectl get cm -o yaml -n kube-system kubeadm-config' to get the kubeadm config directly.[m
[32m+[m[32m  serviceSubnet: 10.96.0.0/12[m
[32m+[m[32m[rishi@master1 ~]$ ps -ef | grep cluster[m
[32m+[m[32mroot      3520  3487  1 02:58 ?        00:13:19 etcd --advertise-client-urls=https://192.168.2.11:2379 --cert-file=/etc/kubernetes/pki/etcd/server.crt --client-cert-auth=true --data-dir=/var/lib/etcd --initial-advertise-peer-urls=https://192.168.2.11:2380 --initial-cluster=master1.localdomain=https://192.168.2.11:2380 --key-file=/etc/kubernetes/pki/etcd/server.key --listen-client-urls=https://127.0.0.1:2379,https://192.168.2.11:2379 --listen-metrics-urls=http://127.0.0.1:2381 --listen-peer-urls=https://192.168.2.11:2380 --name=master1.localdomain --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt --peer-client-cert-auth=true --peer-key-file=/etc/kubernetes/pki/etcd/peer.key --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt --snapshot-count=10000 --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt[m
[32m+[m[32mroot      3546  3504  2 02:58 ?        00:21:35 kube-apiserver --advertise-address=192.168.2.11 --allow-privileged=true --authorization-mode=Node,RBAC --client-ca-file=/etc/kubernetes/pki/ca.crt --enable-admission-plugins=NodeRestriction --enable-bootstrap-token-auth=true --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key --etcd-servers=https://127.0.0.1:2379 --insecure-port=0 --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key --requestheader-allowed-names=front-proxy-client --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt --requestheader-extra-headers-prefix=X-Remote-Extra- --requestheader-group-headers=X-Remote-Group --requestheader-username-headers=X-Remote-User --secure-port=6443 --service-account-issuer=https://kubernetes.default.svc.cluster.local --service-account-key-file=/etc/kubernetes/pki/sa.pub --service-account-signing-key-file=/etc/kubernetes/pki/sa.key --service-cluster-ip-range=10.96.0.0/12 --tls-cert-file=/etc/kubernetes/pki/apiserver.crt --tls-private-key-file=/etc/kubernetes/pki/apiserver.key[m
[32m+[m[32mroot      8938  8895  0 03:08 ?        00:00:30 kube-controller-manager --authentication-kubeconfig=/etc/kubernetes/controller-manager.conf --authorization-kubeconfig=/etc/kubernetes/controller-manager.conf --bind-address=127.0.0.1 --client-ca-file=/etc/kubernetes/pki/ca.crt --cluster-name=kubernetes --cluster-signing-cert-file=/etc/kubernetes/pki/ca.crt --cluster-signing-key-file=/etc/kubernetes/pki/ca.key --controllers=*,bootstrapsigner,tokencleaner --kubeconfig=/etc/kubernetes/controller-manager.conf --leader-elect=true --port=0 --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt --root-ca-file=/etc/kubernetes/pki/ca.crt --service-account-private-key-file=/etc/kubernetes/pki/sa.key --use-service-account-credentials=true[m
[32m+[m[32mrishi    25111  7690  0 15:42 pts/0    00:00:00 grep --color=auto cluster[m
[32m+[m[32m[rishi@master1 ~]$[m
[32m+[m
[32m+[m
[32m+[m[32m[rishi@master1 ~]$ ps -ef | grep cluster-cidr[m
[32m+[m[32mrishi    26167  7690  0 15:43 pts/0    00:00:00 grep --color=auto cluster-cidr[m
[32m+[m[32m[rishi@master1 ~]$ sudo ps -ef | grep cluster-cidr[m
[32m+[m[32mrishi    26494  7690  0 15:43 pts/0    00:00:00 grep --color=auto cluster-cidr[m
[32m+[m[32m[rishi@master1 ~]$ sudo grep cidr /etc/kubernetes/manifests/kube-*[m
[32m+[m[32m[rishi@master1 ~]$[m
[32m+[m
[32m+[m
[32m+[m
[32m+[m[32m[rishi@master1 ~]$ sudo calicoctl node diags[m
[32m+[m[32mCollecting diagnostics[m
[32m+[m[32mUsing temp dir: /tmp/calico346452733[m
[32m+[m[32mDumping netstat[m
[32m+[m[32mDumping routes (IPv4)[m
[32m+[m[32mDumping routes (IPv6)[m
[32m+[m[32mDumping interface info (IPv4)[m
[32m+[m[32mDumping interface info (IPv6)[m
[32m+[m[32mDumping iptables (IPv4)[m
[32m+[m[32mDumping iptables (IPv6)[m
[32m+[m[32mDumping ipsets[m
[32m+[m[32mDumping ipsets (container)[m
[32m+[m[32mCopying journal for calico-node.service[m
[32m+[m[32mDumping felix stats[m
[32m+[m[32mFailed to run command: pkill -SIGUSR1 felix[m
[32m+[m[32mError:[m
[32m+[m[32mCopying Calico logs[m
[32m+[m[32mError creating log directory: mkdir /tmp/calico346452733/diagnostics/logs: file exists[m
[32m+[m
[32m+[m[32mDiags saved to /tmp/calico346452733/diags-20210511_155325.tar.gz[m
[32m+[m[32mIf required, you can upload the diagnostics bundle to a file sharing service[m
[32m+[m[32msuch as transfer.sh using curl or similar.  For example:[m
[32m+[m
[32m+[m[32m    curl --upload-file /tmp/calico346452733/diags-20210511_155325.tar.gz https://transfer.sh//tmp/calico346452733/diags-20210511_155325.tar.gz[m
[32m+[m[32m[rishi@master1 ~]$[m
[32m+[m
[32m+[m
[32m+[m
[32m+[m[32mhi[m
[32m+[m
